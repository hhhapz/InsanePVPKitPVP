package eu.insanepvp.kitpvp

data class Config(
	val name: String,
	val path: String,
	val default: HashMap<String, Any> = hashMapOf())