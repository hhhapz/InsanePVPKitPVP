package eu.insanepvp.kitpvp

object MessageConfig {
	/**
	 * Using lazy so it's only created once
	 */
	val config by lazy {
		ConfigManager.addConfig(Config("messages", "messages"))
	}
	
	/**
	 * Gets a message, and automatically turns into multi lined message + colors
	 */
	fun getMessage(path: String) = config.getStringList(path).c.joinToString(" \n")
	
	/**
	 * Works to replace arguments dynamically.
	 */
	fun getMessageWithArgs(path: String, vararg arguments: String): String {
		var message = config.getStringList(path).c.joinToString(" \n")
		
		for (i in 0 until arguments.size) {
			message = if (i == 0) message.replace("{arg}", arguments[i])
			else message.replace("{arg$i}", arguments[i])
		}
		
		return message
	}
	
}

/**
 * Sets in the config if it doesn't exist. This is used by @see SetMessages#SetMessages()
 */
infix fun List<String>.safeSetTo(path: String) {
	
	if (MessageConfig.config[path] == null)
		MessageConfig.config[path] = this
	
}