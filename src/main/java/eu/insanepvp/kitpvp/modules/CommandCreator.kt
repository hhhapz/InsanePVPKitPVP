package eu.insanepvp.kitpvp.modules

import org.bukkit.command.CommandSender
import org.bukkit.command.defaults.BukkitCommand

/**
 * Handler to create commands without using the plugin.yml
 */
class CommandCreator(name: String,
                     description: String,
                     usageMessage: String,
                     aliases: MutableList<String>,
                     private val executor: CommandHandler?)
	: BukkitCommand(name, description, usageMessage, aliases) {
	
	override fun execute(sender: CommandSender,
	                     commandLabel: String,
	                     args: Array<String>): Boolean {
		executor?.handle(sender, commandLabel, args)
			?: throw IllegalArgumentException("Internal error: Handler not provided!")
		return true
	}
	
}

/**
 * Simple way to create a command. Easy DSL allows the plugin to provide only the info they need
 */
data class CommandCreatorDSL(
	val name: String,
	var description: String = "This is the description for the $name modules",
	var usageMessage: String = "/$name",
	var aliases: MutableList<String> = mutableListOf(),
	var executor: CommandHandler? = null
) {
	
	fun build(): CommandCreator {
		return CommandCreator(name, description, usageMessage, aliases, executor)
	}
}

fun command(name: String,
            construction: CommandCreatorDSL.() -> Unit): CommandCreator {
	val command = CommandCreatorDSL(name)
	command.construction()
	
	return command.build()
}

data class CommandArgument(val name: String,
                           val description: String?,
                           val usageMessage: String?,
                           val aliases: MutableList<String>?)

fun cmdArg(name: String,
        description: String? = null,
        usageMessage: String? = null,
        aliases: MutableList<String>? = null) =
	CommandArgument(name, description, usageMessage, aliases)