package eu.insanepvp.kitpvp.modules

import org.bukkit.Bukkit
import org.bukkit.command.CommandMap

/**
 * This actually registers the command to the server, with reflection
 */
object CommandManager {
	
	private val commands = hashMapOf<String, CommandHandler>()
	
	private val commandMap: CommandMap by lazy {
		val map = Bukkit.getServer().javaClass.getDeclaredField("commandMap")
		map.isAccessible = true
		
		map.get(Bukkit.getServer()) as CommandMap
	}
	
	
	fun loadCommand(cmd: CommandArgument, handler: CommandHandler) {
		
		val command = command(cmd.name) {
			if (cmd.description != null) description = cmd.description
			
			if (cmd.usageMessage != null) usageMessage = cmd.usageMessage
			
			if (cmd.aliases != null) aliases = cmd.aliases
			
			executor = handler
		}
		
		commandMap.register(cmd.name, command)
		
		
	}
	
	
}


