package eu.insanepvp.kitpvp.modules

import org.bukkit.command.CommandSender

interface CommandHandler {
	fun handle(sender: CommandSender, command: String, args: Array<String>)
}