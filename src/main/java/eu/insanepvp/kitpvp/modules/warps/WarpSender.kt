package eu.insanepvp.kitpvp.modules.warps

import eu.insanepvp.kitpvp.*
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent

class WarpSender : Listener {
	//Static objects as the instance may change
	companion object {
		//List of players waiting to be teleported
		private val waiting = mutableListOf<Player>()
		
		/**
		 * This function initiates the whole teleportation process.
		 */
		fun sendPlayer(p: Player, loc: Location, length: Int) {
			//Checks if they are already teleporting, and cancels the new request
			if(waiting.contains(p)) {
				MessageConfig.getMessage("warpAlreadyWaiting") sendTo p
				return
			}
			
			//If no wait, sends them immediately
			if (length == 0) {
				MessageConfig.getMessage("warpComplete") sendTo p
				p.teleport(loc)
			} else {
				//Add them to the waiting list
				waiting.add(p)
				MessageConfig.getMessageWithArgs("warpWait", "$length") sendTo p
				
				//Remove them after the assigned ime.
				Bukkit.getScheduler().scheduleSyncDelayedTask(KitPVP.instance, {
					//This checks if they have moved before the time,
					//as the waiting list removes them on move
					if (waiting.contains(p)) {
						MessageConfig.getMessage("warpComplete") sendTo p
						p.teleport(loc)
					}
					
					waiting.remove(p)
				}, length * 20L)
			}
		}
	}
	
	/**
	 * Checks if players move while waiting to teleport
	 */
	@EventHandler
	fun onMove(ev: PlayerMoveEvent) {
		if (ev.isCancelled) return
		
		val p = ev.player
		
		if (waiting.contains(p) && ev.from.block != ev.to.block) {
			waiting.remove(p)
			MessageConfig.getMessage("warpCancelled") sendTo p
		}
	}
	
}