package eu.insanepvp.kitpvp.modules.warps

import eu.insanepvp.kitpvp.MessageConfig
import eu.insanepvp.kitpvp.modules.CommandHandler
import eu.insanepvp.kitpvp.sendTo
import org.bukkit.command.CommandSender

/**
 * Handling users removing a warp
 */
object DelwarpHandler : CommandHandler {
	override fun handle(sender: CommandSender, command: String, args: Array<String>) {
		
		//They must have access to the command
		if (!sender.hasPermission("insanepvpkitpvp.warp.remove")) {
			MessageConfig.getMessage("warpNoPermissionRemove") sendTo sender
			return
		}
		
		//They need to provide the name of the warp
		if (args.isEmpty()) {
			MessageConfig.getMessage("warpRemovedNotProvided") sendTo sender
			return
		}
		
		//The name of the warp is the first component
		val warpName = args.component1()
		
		//Make sure the warp exists
		if (WarpManager.getWarp(warpName) == null) {
			MessageConfig.getMessageWithArgs("warpRemovedNotExist", warpName) sendTo sender
			return
		}
		
		//Remove the warp, and let the user know
		WarpManager.delWarp(warpName)
		MessageConfig.getMessageWithArgs("warpRemoved", warpName) sendTo sender
	}
	
}