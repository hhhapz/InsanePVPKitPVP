package eu.insanepvp.kitpvp.modules.warps

import eu.insanepvp.kitpvp.*
import eu.insanepvp.kitpvp.modules.CommandManager
import eu.insanepvp.kitpvp.modules.cmdArg
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.configuration.file.FileConfiguration
object WarpManager {
	
	private val warps = hashMapOf<String, Location>()
	
	/**
	 * The configuration is lateinit as it's gotten every time warps are loaded.
	 * This makes it easy for plugin reload to reload everything.
	 */
	private lateinit var config: FileConfiguration
	
	fun loadWarps() {
		//Setup the config file
		config = ConfigManager.addConfig(Config("warps", "warps", hashMapOf()))
		
		//Getting all warps from the configuration file
		if(config.isConfigurationSection("warps"))
		config.getConfigurationSection("warps").getKeys(false).forEach { name ->
			//Convenience
			val prefix = "warps.$name"
			
			
			val loc = config.get(prefix) as Location
			
			//Add to the warp list
			warps[name] = loc
		}
		
		//Setup Listeners
		Bukkit.getServer().pluginManager.registerEvents(WarpSender(), KitPVP.instance)
		
		//Setup the warp modules
		CommandManager.loadCommand(cmdArg("warp", "Allow players to warp to a location", "/warp <warpName>", mutableListOf("warps")), WarpHandler)
		CommandManager.loadCommand(cmdArg("setwarp", "Allow players set a warp to a location", "/setwarp <warpName>"), SetwarpHandler)
		CommandManager.loadCommand(cmdArg("delwarp", "Allow players to remove a warp", "/delwarp <warpName>"), DelwarpHandler)
	}
	
	/**
	 * Get a warp by the name of the warp.
	 * Returns null if it doesn't exist
	 */
	fun getWarp(name: String) = warps[name]
	
	/**
	 * Get all warp names
	 */
	fun getWarps(): MutableSet<String> {
		return if(warps.keys.isNotEmpty()) warps.keys
		else return mutableSetOf("No warps found")
	}
	
	/**
	 * Add a warp to the warp system, and save
	 */
	fun addWarp(name: String, loc: Location) {
		warps[name] = loc
		saveWarps()
	}
	
	/**
	 * Remove a warp from the warp system, and save
	 */
	fun delWarp(name: String) {
		warps.remove(name)
		saveWarps()
	}
	
	
	/**
	 * Save all warps into the config
	 */
	fun saveWarps() {
		config["warps"] = null
		warps.keys.forEach { name ->
			config["warps.$name"] = warps[name]
		}
		
		ConfigManager.saveConfig("warps")
	}
}