package eu.insanepvp.kitpvp.modules.warps

import eu.insanepvp.kitpvp.MessageConfig
import eu.insanepvp.kitpvp.modules.CommandHandler
import eu.insanepvp.kitpvp.sendTo
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

/**
 * Handling users adding a warp
 */
object SetwarpHandler : CommandHandler {
	override fun handle(sender: CommandSender, command: String, args: Array<String>) {
		
		
		
		// You can't add a warp from console, you have no location (lol)
		if (sender !is Player) {
			"&cYou must be a player to use the warp commands!" sendTo sender
			return
		}
		
		//Make sure they can actually access the command
		if (!sender.hasPermission("insanepvpkitpvp.warp.set")) {
			MessageConfig.getMessage("warpNoPermissionSet") sendTo sender
			return
		}
		
		//They must give a name for the warp. If the warp alraedy exists, it simple will overwrite
		if (args.isEmpty()) {
			MessageConfig.getMessage("warpSetNotProvided") sendTo sender
			return
		}
		//The warp is the first argument they provide
		val warpName = args.component1()
		
		//Add the warp, and let them know it has happened
		WarpManager.addWarp(warpName, sender.location)
		MessageConfig.getMessageWithArgs("warpSet", warpName) sendTo sender
	}
	
}