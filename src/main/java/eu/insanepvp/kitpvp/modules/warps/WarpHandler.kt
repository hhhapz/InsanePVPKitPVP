package eu.insanepvp.kitpvp.modules.warps

import eu.insanepvp.kitpvp.*
import eu.insanepvp.kitpvp.modules.CommandHandler
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

/**
 * This handles both /warp, as well as /warps. The latter lists warps only,
 * the former lists warps when no arguments are provided, and teleports when
 * an argument is provided
 */
object WarpHandler : CommandHandler {
	override fun handle(sender: CommandSender, command: String, args: Array<String>) {
		
		if(command == "warps") {
			listWarps(sender)
			return
		}
		//To warp, you must be a player
		if (sender !is Player) {
			"&cYou must be a player to use the warp commands!" sendTo sender
			return
		}
		//When either is chosen
		
		when (command) {
			"warp"  -> {
				//If the arguments are empty, list them out to the user
				if (args.isEmpty()) {
					listWarps(sender)
					return
				}
				
				//Gets the warp name the user would like to go to
				val warp = args.component1()
				val warpLocation = WarpManager.getWarp(warp)
				
				//Make sure it exists
				if (warpLocation == null) {
					MessageConfig.getMessageWithArgs("warpNotFound", warp) sendTo sender
					return
				}
				
				//Make sure they have access to the warp
				if (!sender.hasPermission("insanepvp.warp.$warp")) {
					MessageConfig.getMessageWithArgs("warpNoPermission", warp) sendTo sender
					return
				}
				
				
				val config = KitPVP.instance.config
				//Get the time they need to wait. Is zero for override
				val lengthToWait = if (sender.hasPermission(config.getString("warps.timing.overridePerm"))) 0
				else config.getInt("warps.timing.length")
				
				//Send'em
				WarpSender.sendPlayer(sender, warpLocation, lengthToWait)
				
			}
			
			
		}
		
	}
	
	private fun listWarps(p: CommandSender) {
		val delimiter = KitPVP.instance.config.getString("warps.listwarps.delimiter")
		val message = MessageConfig.getMessageWithArgs("warpList", WarpManager.getWarps().joinToString(delimiter))
		message sendTo p
	}
}