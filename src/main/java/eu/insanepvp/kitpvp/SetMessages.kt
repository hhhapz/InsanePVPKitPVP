package eu.insanepvp.kitpvp

/**
 * Setting up all the custom messages needed
 */
fun setupCustomMessages() {
	mutableListOf("&cList of all warps:", "&a{arg}", "", "&6Please use /warp <warp> to teleport.") safeSetTo "warpList"
	
	mutableListOf("&cThe warp {arg} was not found!") safeSetTo "warpNotFound"
	
	mutableListOf("The teleport has been cancelled") safeSetTo "warpCancelled"
	
	mutableListOf("Please wait &6{arg}&r seconds for your teleport. Do not move!") safeSetTo "warpWait"
	
	mutableListOf("&cYou are already waiting to be warped! Please wait.") safeSetTo "warpAlreadyWaiting"
	
	mutableListOf("&6Teleporting...") safeSetTo "warpComplete"
	
	mutableListOf("&aThe warp {arg} has been set") safeSetTo "warpSet"
	
	mutableListOf("&aPlease provide a warp name! /setwarp [warpName]") safeSetTo "warpSetNotProvided"
	
	mutableListOf("&aThe warp {arg} has been removed") safeSetTo "warpRemoved"
	
	mutableListOf("&aPlease provide a warp name! /delwarp [warpName]") safeSetTo "warpRemovedNotProvided"
	
	mutableListOf("&cThe warp {arg} does not exist!") safeSetTo "warpRemovedNotExist"
	
	mutableListOf("&cYou don't have access to warp {arg}!") safeSetTo "warpNoPermission"
	
	mutableListOf("&cYou don't have access to that modules!") safeSetTo "warpNoPermissionSet"
	
	mutableListOf("&cYou don't have access to that modules!") safeSetTo "warpNoPermissionRemove"
	
	ConfigManager.saveConfig("messages")
}
