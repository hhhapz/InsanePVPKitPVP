package eu.insanepvp.kitpvp

import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import java.io.File

object ConfigManager {
	
	private val configs = mutableMapOf<Config, FileConfiguration>()
	private val configFiles = mutableMapOf<Config, File>()
	
	/**
	 * Add configuration file to the configurations
	 */
	fun addConfig(config: Config): YamlConfiguration {
		//Add .yml to the path if it isn't ending with yml
		val path = if (config.path.endsWith(".yml")) config.path
		else config.path + ".yml"
		
		//Create the file in the plugin data folder
		val file = File(KitPVP.instance.dataFolder, path)
		
		//Check if file exists. if it does not, make new true,
		// so we can add defaults (if any exist)
		val new = if (!file.exists()) {
			
			file.parentFile.mkdirs()
			file.createNewFile()
			
			true
		} else false
		
		//Load configuration files and add it to the configuration files list
		val configFile = YamlConfiguration.loadConfiguration(file)
		configs[config] = configFile
		configFiles[config] = file
		
		//If the configuration file is newly created, add the default data
		if (new) {
			
			config.default.forEach { newPath, value ->
				configFile[newPath] = value
			}
		}
		
		configFile.save(file)
		
		//Return config file for use
		return configFile
	}
	
	/**
	 * Get list of all configuration files
	 */
	fun getConfigs() = configs.values
	
	/**
	 * Get a configuration file from the string,
	 * which is the name of the name file. Returns null if doesn't exist
	 */
	fun getConfig(name: String) = configs[getConfigByString(name)]
	
	/**
	 * Get a configuration file from the configuration object.
	 * Returns null if doesn't exist
	 */
	fun getConfig(config: Config) = configs[config]
	
	fun saveConfig(name: String) {
		val config = getConfigByString(name)
			?: throw IllegalArgumentException("Configuration not found!")
		
		configs[config]?.save(configFiles[config])
			?: throw IllegalArgumentException("Configuration not in system!")
	}
	
	/**
	 * Get Config object from String
	 */
	fun getConfigByString(name: String) = configs.keys.find { it.name == name }
}