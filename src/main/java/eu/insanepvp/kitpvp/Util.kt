package eu.insanepvp.kitpvp

import org.bukkit.ChatColor
import org.bukkit.command.CommandSender

infix fun String.sendTo(sender: CommandSender) {
	sender.sendMessage(c)
}

val String.c: String
	get() {
		return ChatColor.translateAlternateColorCodes('&', this)
	}

val List<String>.c: List<String>
	get() {
		
		val tempList = mutableListOf<String>()
		forEach { tempList.add(it.c) }
		return tempList
	}