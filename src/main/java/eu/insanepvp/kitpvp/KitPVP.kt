package eu.insanepvp.kitpvp

import eu.insanepvp.kitpvp.modules.warps.WarpManager
import org.bukkit.plugin.java.JavaPlugin

class KitPVP : JavaPlugin() {
	
	/**
	 * Singleton instance for the plugin object
	 */
	companion object {
		lateinit var instance: KitPVP
	}
	
	/**
	 * Start point of plugin application
	 */
	override fun onEnable() {
		instance = this
		
		config.options().copyDefaults(true)
		
		saveConfig()
		
		loadModules()
	}
	
	/**
	 * Load all modules here! :)
	 */
	fun loadModules() {
		setupCustomMessages()
		WarpManager.loadWarps()
	}
	
	override fun onDisable() {
	}
	
}